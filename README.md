Sheep for ncurses
----------------------------------------------------------------------------
Sheep for ncueses is an implementation [sheep] for ncueses.

ASCII Art
----------------------------------------------------------------------------
```
           @
........../|..........................
         @
        /|
       @               (・＠)
      /|
     @
    ||
```

License
----------------------------------------------------------------------------
[WTFPL] 



[sheep]: https://bitbucket.org/runne/woolmark "Sheep"
[WTFPL]: http://www.WTFPL.net "WTFPL"
