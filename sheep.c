/**
 * sheep.c for sheep.
 */

#include <stdlib.h>
#include <curses.h>
#include <signal.h>
#include "sheep.h"

int woolmark_sheep_number = 0;

int **woolmark_sheep_pos;

int woolmark_sheep_add;

void woolmark_handle_signal()
{
    woolmark_sheep_add = 1;
}

/**
 * my mvaddstr function.
 *
 * @param y y position
 * @param x x position
 * @param str string to draw
 */
int woolmark_mvaddstr(int y, int x, char *str)
{
    for ( ; x < 0; ++x, ++str) {
        if (*str == '\0')  return ERR;
    }
    for ( ; *str != '\0'; ++str, ++x) {
        if (mvaddch(y, x, *str) == ERR)  return ERR;
    }
    return OK;
}

/**
 * main function.
 *
 * @param argc argument length
 * @param argv argument values
 */
int main(int argc, char *argv[])
{
    int x, i;

    woolmark_sheep_add = 0;

    woolmark_sheep_pos = (int **)(malloc(sizeof(int *) * 100));
    for(i = 0; i < 100; i++) {
        woolmark_sheep_pos[i] = (int *)(malloc(sizeof(int) * 2));
        woolmark_sheep_pos[i][0] = 0;
        woolmark_sheep_pos[i][1] = -1;
    }

    woolmark_sheep_pos[0][0] = COLS;
    woolmark_sheep_pos[0][1] = 4;

    initscr();
    signal(SIGINT, woolmark_handle_signal);
    noecho();
    curs_set(0);
    leaveok(stdscr, TRUE);
    scrollok(stdscr, FALSE);

    while(1) {
        woolmark_draw_fence();

        for(i = 0; i < 100; i++) {
            if(woolmark_sheep_pos[i][1] < 0) {

                if(woolmark_sheep_add == 0) {
                    continue;
                }

                else if(woolmark_sheep_add == 1) {
                    woolmark_sheep_pos[i][0] = COLS;
                    woolmark_sheep_pos[i][1] = rand() % 7;

                    woolmark_sheep_add = 0;
                }

            }

            woolmark_sheep_pos[i][0]--;

            if(woolmark_sheep_pos[i][0] < SHEEPLENGTH * -1) {
                if(i == 0) {
                    woolmark_sheep_pos[i][0] = COLS;
                } else {
                    woolmark_sheep_pos[i][1] = -1;
                }
            }

            else {
                woolmark_draw_sheep(
                        woolmark_sheep_pos[i][0],
                        woolmark_sheep_pos[i][1]);
            }
        }

        refresh();
        usleep(100000);
    }
    mvcur(0, COLS - 1, LINES - 1, 0);
    endwin();
}

int woolmark_draw_fence()
{
    static char *fence[FENCEHEIGHT] = {
        FENCE1, FENCE2, FENCE3, FENCE4, FENCE5, FENCE6, FENCE7, FENCE8 };
    int i, j;

    // draw the fence
    for(i = 0; i < FENCEHEIGHT; i++) {
        if(i == 1) {
            for(j = 0; j < COLS; j++) {
                if(mvaddch(LINES - FENCEHEIGHT + i, j, '.') == ERR) break;
            }
        }

        woolmark_mvaddstr(LINES - FENCEHEIGHT + i,
                COLS / 2 - FENCEWIDTH, fence[i]);
    }
}

/**
 * run and draw the sheep.
 *
 * @param x x position
 * @param y y position
 * @return running status
 */
int woolmark_draw_sheep(int x, int y)
{
    char *sheep_number;

    // break if sheep goes away
    if (x < - SHEEPLENGTH)  return ERR;

    // draw a sheep
    if(x > COLS / 2 - 1 - FENCEWIDTH && x < COLS / 2 + 8 - FENCEWIDTH) {
        woolmark_mvaddstr(LINES - y - 1, x, SHEEP);
    } else {
        woolmark_mvaddstr(LINES - y, x, SHEEP);
    }

    // count up
    if(x == COLS / 2) {
        woolmark_sheep_number++;
    }

    // draw sheep counter
    sheep_number = (char *)malloc(sizeof(char) * 100);
    sprintf(sheep_number, "%d sheep", woolmark_sheep_number);
    woolmark_mvaddstr(1, 1, sheep_number);
    free(sheep_number);

    return OK;
}

// vim: et ts=4 sw=4
