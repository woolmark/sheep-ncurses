/**
 * sheep.h for sheep.
 */

#define SHEEPHEIGHT      1
#define SHEEPLENGTH      8

#define FENCEHEIGHT      8
#define FENCEWIDTH      13

#define SHEEP  "(* @) "

#define FENCE1 "         @   "
#define FENCE2 "......../|..."
#define FENCE3 "       @     "
#define FENCE4 "      /|     "
#define FENCE5 "     @       "
#define FENCE6 "    /|       "
#define FENCE7 "   @         "
#define FENCE8 "  ||         "

